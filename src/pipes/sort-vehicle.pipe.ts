import { Pipe, Injectable } from '@angular/core';

@Pipe({
    name: 'sortVehicle'
  })
  @Injectable()

  export class SortVehiclePipe {
  
    transform(value: any,filterString?: any): boolean {
        if(filterString == ""){
          return true;
        }
        else {
       
          filterString = filterString.toLowerCase();
          value = value.toString().toLowerCase();
          return value.includes(filterString);
        }
    }     
  }
  