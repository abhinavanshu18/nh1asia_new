import { Component } from '@angular/core';
// import { DriverDetail } from '../../model/driver-detail';
import { ModalController, NavParams, NavController, ToastController, } from 'ionic-angular';
import { UserDetailPage } from '../user-detail/user-detail';
import { DriverLocationPage } from '../driver-location/driver-location';
import { DriverDetailsService } from '../../services/driver-details.service';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../../services/data.service';
import { Transporter } from '../../model/transporter';
import { SessionService } from '../../services/session.service';
import { SignOutService } from '../../services/sign-out.service';
import { UserProfilePage } from '../user-profile/user-profile';

@Component({
  selector: 'page-truck-list',
  templateUrl: 'truck-list.html'
})
export class TruckListPage {
  listOfTrucks: any;
  dropdownVisibilityStatus: any = [];
  locationName: any = [];
  searchText: string = '';
  transporter: Transporter = new Transporter();
  baseUrl: string;
  transporterObj:any;

  constructor(private modalCtrl: ModalController
    , private navCtrl: NavController
    , private navParams: NavParams
    , private driverDetail: DriverDetailsService
    , private http: HttpClient
    , private toastCtrl: ToastController
    , private dataService: DataService
    , private session: SessionService,
     private signOutService: SignOutService) {
    this.baseUrl = this.session.BaseUrl;

    // retrieve trucks list for the transporter
    let id = +this.navParams.get("id");
    this.transporterObj = +this.navParams.get("transporter");
    
    this.dataService.getDetails(id).subscribe(
      trucks => {
        debugger;
        this.listOfTrucks = trucks;
        this.transporter.trucks = trucks || [];
        this.loadLocations();
        console.log("Transporter Details: ",this.transporter);
        for (let i = 0; i < this.transporter.trucks.length; i++) {
          this.dropdownVisibilityStatus[i] = false;
          this.locationName[i] = '';
        }
      }
      , error => {
        console.log("Trucks error: " + error);
      }
    );
  }
  loadLocations() {
    this.dataService.getTrucksPosition().subscribe(
      status => {
        if (status === true) {
          debugger;
          new Promise((resolve, reject) => {
            this.transporter.trucks.forEach(truck => {
              this.getTruckLocationInfo(truck);
            });
          });
        }
      }
    );
  }
  userDetail(truckDetails: any) {
    console.log("Userdetails ",truckDetails);
    let userDetailModal = this.modalCtrl.create(UserDetailPage, { name: truckDetails.driverName, aadhar: truckDetails.aadhar, licenseNum: truckDetails.licenseId, address: truckDetails.address, profilePic: truckDetails.profilePic });
    userDetailModal.present();
  }
  dropdownVisibility(index: number, locationObj?: any) {
    if (locationObj != undefined) {
      this.truckLocationName(index, locationObj);
    }
    if (this.dropdownVisibilityStatus[index] == true) {
      this.dropdownVisibilityStatus[index] = false;
    }
    else {
      this.dropdownVisibilityStatus[index] = true;
    }
  }
  truckLocation(locationData,deviceId) {
    if (locationData === undefined || locationData === "" || locationData == null) {
      this.showToast("Location not available!");
      return;
    }
    let locationLatLong = locationData.split(",");
    let truckLocationModal = this.modalCtrl.create(DriverLocationPage, { deviceId: deviceId, latitude: locationLatLong[0], longitude: locationLatLong[1] });
    truckLocationModal.present();
  }
  getTruckLocationInfo(truck) {
    let locationLatLong = truck.location.split(",");
    let googleGeoApiUrl = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + truck.location + "&sensor=true";
    let locationNameArr: any;
    this.http.get(googleGeoApiUrl).subscribe(
      data => {
        locationNameArr = data["results"];
        // if address not found
        if (locationNameArr.length == 0){
          truck.locationName = "Location name not retrieved";
          return;
        } 

        if (locationNameArr[locationNameArr.length - 3].formatted_address.length > 23) {
          truck.locationName = locationNameArr[locationNameArr.length - 3].formatted_address.substring(0, 22) + "...";
        }
        else {
          truck.locationName = locationNameArr[locationNameArr.length - 3].formatted_address;
        }
      }
    );
  }
  truckLocationName(index, location) {
    let locationLatLong = location.split(",");
    let googleGeoApiUrl = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + location + "&sensor=true";
    let locationNameArr: any;
    if(location != ""){
      this.http.get(googleGeoApiUrl).subscribe(
        data => {
          locationNameArr = data["results"];
          debugger;
          if (locationNameArr[locationNameArr.length - 3].formatted_address.length > 23) {
            this.locationName[index] = locationNameArr[locationNameArr.length - 3].formatted_address.substring(0, 22) + "...";
          }
          else {
            this.locationName[index] = locationNameArr[locationNameArr.length - 3].formatted_address;
          }
        }
      );
    }
    else{
      this.locationName[index] = "Location not retrieved";
    }

  }
  signout(){
    this.signOutService.signout(this.navCtrl);
  }
  userProfileDisplay(){
    let truckLocationModal = this.modalCtrl.create(UserProfilePage, {  transporterName: this.session.User.transporterName,aadhar: this.session.User.aadhar,transporterContactNumber: this.session.User.transporterContactNumber, profilePic: this.session.User.profilePic});
    truckLocationModal.present();   
  }
  showToast(msg: string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'top',
      showCloseButton: true,
      dismissOnPageChange: true
    });
    toast.present();
  }
}