import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { SessionService } from '../../services/session.service';

@Component({
  selector: 'page-user-profile',
  templateUrl: 'user-profile.html',
})
export class UserProfilePage {

  transporterContactNumber: any;
  baseUrl: string;
  aadhar:number;
  name:string;
  profilePic:string;

 constructor(private navParams:NavParams,private viewCtrl: ViewController
             , private session: SessionService){ 
  this.name = this.navParams.get('transporterName');
  this.aadhar = this.navParams.get('aadhar');
  this.transporterContactNumber = this.navParams.get('transporterContactNumber');
  this.profilePic = this.navParams.get('profilePic');
  this.baseUrl = this.session.BaseUrl;
 }
 closeModal() {
  this.viewCtrl.dismiss();
}
 
}
