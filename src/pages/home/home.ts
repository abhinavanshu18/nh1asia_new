import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { TruckListPage } from '../truck-list/truck-list';
import { DriverDetailsService } from '../../services/driver-details.service';
import { AuthService } from '../../services/auth.service';
// import { AppVersion } from '@ionic-native/app-version';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit{

  appVersionCode: Promise<string>;
  appPackageName: Promise<string>;
  appVersionNumber: Promise<string>;
  appAppName: Promise<string>;
  otpGenerationSuccess: boolean = false;
  otpNumber1: any;
  otpNumber2: any;
  otpNumber3: any;
  otpNumber4: any;
  appendedOtp: string;
  phoneNumber: string;
  submitBtnText: string = 'Submit';
  dummyUUID = this.authService.UUID;

  constructor(private navCtrl: NavController
    , private driverDetail: DriverDetailsService
    , private toastCtrl: ToastController
    , private authService: AuthService
    // private appVersion: AppVersion
  ) {
  }

  ngOnInit(): void {
  //  this.appVersionNumber = this.appVersion.getVersionNumber()
  //  this.appAppName = this.appVersion.getAppName()
  //  this.appPackageName = this.appVersion.getPackageName()
  //  this.appVersionCode = this.appVersion.getVersionCode()
  }
  onSubmit(form: NgForm) {
    console.log("appendedOtp :", this.appendedOtp);
    if (this.submitBtnText == "Submit") {
      debugger;
      this.authService.verifyPhoneNo(this.phoneNumber.toString()).subscribe(
        status => {
          this.otpGenerationSuccess = true;
          this.submitBtnText = "Verify OTP";
          this.showToast("OTP generate successfully");
      },
        error => {
          this.showToast(error.value);
        });
    }
    else if (this.submitBtnText == "Verify OTP" && this.otpGenerationSuccess == true) {
      this.appendedOtp = ((this.otpNumber1 || this.otpNumber2 || this.otpNumber3 || this.otpNumber4) == undefined) ? "" : this.otpNumber1 + this.otpNumber2 + this.otpNumber3 + this.otpNumber4;
      // Add OTP verification call
      this.authService.verifyOtp(this.appendedOtp, this.phoneNumber).subscribe(
        transporter => {       
          this.navCtrl.push(TruckListPage, {transporterObj:transporter,id: transporter.transporterId});          
        }
        ,error => {
          this.showToast(error.value);
        }
      );
    }
  }
  restrictToNumber(phoneNumber){
    phoneNumber = Number(phoneNumber) 
    let originalPhoneNum = phoneNumber;
    phoneNumber = phoneNumber%10;
    if(phoneNumber.toString() != "NaN"){
      this.phoneNumber = originalPhoneNum;
    }
  }
  restrictToOneEntry(otpNumber, focusOnIdNum ,backFocusOnIdNum) {
    if(otpNumber != ""){
      otpNumber = Number(otpNumber)
    if (otpNumber >= 0 && otpNumber <= 9 && otpNumber != null && focusOnIdNum!=1) {
      setTimeout(function () {
        document.getElementById("otpTextArea" + focusOnIdNum).focus();
      }, 75);
    }
    if(otpNumber.toString() == "NaN" ){
      if (focusOnIdNum == 2) {
        this.otpNumber1 = null;
      }
      else if (focusOnIdNum == 3) {
        this.otpNumber2 = null;
      }
      else if (focusOnIdNum == 4) {
        this.otpNumber3 = null;
      }
    }
  }
  else if(otpNumber == "" && backFocusOnIdNum != 4){
    setTimeout(function () {
      document.getElementById("otpTextArea" + backFocusOnIdNum).focus();
    }, 75);
  }
  }
  ionViewDidLeave() {
    this.otpGenerationSuccess = false;
    this.phoneNumber = null;
    this.submitBtnText = 'Submit';
    console.log("OTP4 :",this.otpNumber4);
    this.otpNumber1 = null;
    this.otpNumber2 = null;
    this.otpNumber3 = null
    this.otpNumber4 = null
    setTimeout(function () {
      this.otpNumber4 = null;
    }, 50);
  }

  showToast(msg:string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'top',
      showCloseButton: true,
      dismissOnPageChange: true
    });
    toast.present();
  }
  regenerateOTP(phoneNumber){
    this.otpNumber1 = null;
    this.otpNumber2 = null;
    this.otpNumber3 = null
    this.otpNumber4 = null;

    setTimeout(function () {
      document.getElementById("otpTextArea1").focus();
    }, 2001);
   
    this.authService.verifyPhoneNo(this.phoneNumber.toString()).subscribe(
      status => {
        this.otpGenerationSuccess = true;
        this.submitBtnText = "Verify OTP";
        this.showToast("OTP generate successfully");
    },

      error => {
       this.showToast(error.value);
      });
  }
}
