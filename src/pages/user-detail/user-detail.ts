import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { SessionService } from '../../services/session.service';

@Component({
  selector: 'page-user-detail',
  templateUrl: 'user-detail.html',
})
export class UserDetailPage {

  baseUrl: string;
  address:string;
  licenseNum:string;
  aadhar:number;
  name:string;
  profilePic:string;

 constructor(private navParams:NavParams,private viewCtrl: ViewController
, private session: SessionService){ 
  this.licenseNum = this.navParams.get('licenseNum');
  this.name = this.navParams.get('name');
  this.aadhar = this.navParams.get('aadhar');
  this.address = this.navParams.get('address');
  this.profilePic = this.navParams.get('profilePic');
  this.baseUrl = this.session.BaseUrl;
 }
 closeModal() {
  this.viewCtrl.dismiss();
}
 
}
