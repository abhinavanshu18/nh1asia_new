import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DriverLocationPage } from './driver-location';

@NgModule({
  declarations: [
    DriverLocationPage,
  ],
  imports: [
    IonicPageModule.forChild(DriverLocationPage),
  ],
})
export class DriverLocationPageModule {}
