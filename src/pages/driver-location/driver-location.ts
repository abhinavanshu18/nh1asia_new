import { Component, transition } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import 'rxjs/add/operator/takeWhile';
import { google } from '@agm/core/services/google-maps-types';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'page-driver-location',
  templateUrl: 'driver-location.html',
})
export class DriverLocationPage {

  deviceId: number;
  truckLatLng: number = 861693034670570;
  delay: number;
  deltaLng: number;
  deltaLat: number;
  i: number;
  step: any;
  marker: any;
  distance: any;
  latLngText: any;
  webConsoleService: any;
  alive: any;
  latitude: number;
  longitude: number;
  count:number;
  clrInt:any;
  public icon = {
    url: "assets/imgs/small_truck_new.png",
    scaledSize: {
      height: 25,
      width: 30
    }
  };
  constructor(private navParams: NavParams, private viewCtrl: ViewController,private dataService:DataService) {
    this.latitude = +this.navParams.get('latitude');
    this.longitude = +this.navParams.get('longitude');
    this.deviceId = this.navParams.get('deviceId');

    setInterval (() => {
      this.dataService.getSingleTruckPosition(this.deviceId).subscribe(
        status => {
          this.latitude = status[0];
          this.longitude = status[1];
        }
      );
    }, 4000)
  }
  ionViewDidLoad() {

  }

  closeModal() {
    this.viewCtrl.dismiss();
  }


}
