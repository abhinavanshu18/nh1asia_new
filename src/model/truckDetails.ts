import { Location } from "./location";

export class Truck {
    constructor() {
    }
    public id: number;
	/*
	 * truck details
	 */
    public registrationNo: String;
    public rcCopy: string;
    public insuranceCopy: string;
    public loanCopy: string;
    public permitCopy: string;
    public transporterId: number;
    public status: boolean;
	/*
	 * driver details
	 */
    public driverName: string;
    public driverContact: string;
    public licenseId: string;
    public aadhar: string;
    public address: string;
    public profilePic: string;
	/*
	 * device details
	 */
    public deviceId: string;
    public serialNo: string;
    public model: string;
    public location: string;
    public locationName: string;
}
// export class TruckDetails{
//     constructor(public truckNumber:string,
//         public location:Location,
//         public name:string,
//         public contactNumber:number,
//         public address:string,
//         public licenseNum:string,
//         public profilePic:string

//         ,public registrationNo:string
//         ,public rcCopy:string
//         ,public insuranceCopy:string
//         ,public loanCopy:string
//         ,public permitCopy:string
//         ,public transporterId:number
//         ,public status:boolean) {
//     }
// }
