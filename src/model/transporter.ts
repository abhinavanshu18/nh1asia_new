import { Truck } from "./truckDetails";

export class Transporter {
	public id: number;
	public transporterName: string;
	public transporterContactNumber: string;
	public aadhar: string;
	public customerId: string;
	public status: boolean;
	public profilePic: string;
	public createdBy: string;
	public createdDate?: any;
	public updatedBy?: any;
	public updatedDate?: any;
	public trucks: Array<Truck>;

	// id: number;
	// transporterName: string;
	// transporterContactNumber: string;
	// aadhar: string;
	// customerId: string;
	// status: boolean;
	// profilePic?: any;
	// createdBy?: any;
	// createdDate?: any;
	// updatedBy?: any;
	// updatedDate?: any;
	// trucks?: any;
	constructor() {
		this.trucks = new Array<Truck>();
	}
}