import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Ng2FilterPipeModule } from 'ng2-filter-pipe';
import { AgmCoreModule } from '@agm/core';
import { HttpModule, Http, RequestOptions } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';

import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TruckListPage } from '../pages/truck-list/truck-list';
// import { DataProvider } from '../providers/data/data';
import { UserDetailPage } from '../pages/user-detail/user-detail';
import { DriverLocationPage } from '../pages/driver-location/driver-location';
import { SortVehiclePipe } from '../pipes/sort-vehicle.pipe';
import { AuthService } from '../services/auth.service';
import { SessionService } from '../services/session.service';
import { DriverDetailsService } from '../services/driver-details.service';
import { DataService } from '../services/data.service';
import { SignOutService } from '../services/sign-out.service';
import { AuthHttp } from 'angular2-jwt';
import { authHttpServiceFactory } from '../services/authServiceFactory';
import { UserProfilePage } from '../pages/user-profile/user-profile';
// import { AppVersion } from '@ionic-native/app-version'

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TruckListPage,
    UserDetailPage,
    DriverLocationPage,
    SortVehiclePipe,
    UserProfilePage
  ],
  imports: [
    BrowserModule,
    Ng2FilterPipeModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA311BdkfzsNAQST-VfMpGNiH55TMaLh1o'
    }),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TruckListPage,
    UserDetailPage,
    DriverLocationPage,
    UserProfilePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    // AppVersion,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DriverDetailsService,
    {provide: AuthHttp, useFactory: authHttpServiceFactory, deps: [Http,RequestOptions]},
    ,AuthService
    ,UniqueDeviceID
    ,SessionService
    ,DataService,
    SignOutService
  ]
})
export class AppModule {}
