import { Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DriverDetailsService{
    Uri:string = 'https://nh1asia2.firebaseio.com/users/';

    constructor(private http: HttpClient) {
    }
    
    apiUrl:string;
    userPhoneNumber:any;
    notValidateUser:boolean = false;
    // apiUrl = "https://nh1asia2.firebaseio.com/groups/9111111111.json";
    
    // apiUrl = "https://nh1asia2.firebaseio.com/driverDetails.json";
    
    getDriverDetails(phoneNumber): Observable<any> {
        this.apiUrl = `${this.Uri}${phoneNumber}.json`;
        return this.http.get(
            this.apiUrl
            , { responseType: 'json' }
        );
    }
}