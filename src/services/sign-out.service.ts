import { Injectable} from '@angular/core';
import { AlertController } from 'ionic-angular';
import { TOKEN_NAME } from './auth.constant';

@Injectable()
export class SignOutService{

    constructor(private alertCtrl: AlertController) {
    }
    signout(navCtrl) {
        let alert = this.alertCtrl.create({
          title: 'Sign Out',
          message: 'Are you sure you want to signout ?',
          buttons: [
            {
                text: 'Yes',
                handler: () => {
                    localStorage.setItem(TOKEN_NAME,null);
                    navCtrl.popToRoot();
                }
              },
            {
              text: 'No',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            }       
          ]
        });
        alert.present();
      } 
}