import { Injectable} from '@angular/core';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { Http, Response, RequestOptions, Headers, URLSearchParams } from '@angular/http'; 
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http'; 
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Transporter } from '../model/transporter';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable()
export class SessionService {
    User: Transporter = null;
    // BaseUrl: string = "http://13.127.193.228:9001/";
    BaseUrl: string = "http://13.232.41.205:9001/";

    constructor() {

    }
}