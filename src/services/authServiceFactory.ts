import { Http, RequestOptions } from "@angular/http";
import { AuthConfig, AuthHttp } from "angular2-jwt";
import { TOKEN_NAME } from "./auth.constant";

export function authHttpServiceFactory(http: Http,options:RequestOptions) {
    var httpI = new AuthHttp(new AuthConfig({
      headerPrefix: 'Bearer Token',
      tokenName: TOKEN_NAME,      
      globalHeaders: [{'Content-Type': 'application/json'},{'Authorization':'Bearer '+ localStorage.getItem(TOKEN_NAME)}],
      noJwtError: false,
      noTokenScheme: true,
      // tokenGetter: (() => localStorage.getItem(TOKEN_NAME)),
    }), http,options);
    console.log(httpI);
    return httpI;
  }