import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers, URLSearchParams, RequestOptionsArgs } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Transporter } from '../model/transporter';
import { catchError, map, tap } from 'rxjs/operators';
import { SessionService } from './session.service';
import { Truck } from '../model/truckDetails';
// import { AuthHttp } from 'angular2-jwt';
import { TOKEN_AUTH_PASSWORD, TOKEN_AUTH_USERNAME, TOKEN_NAME } from '../services/auth.constant';

@Injectable()
export class DataService {
    // Uri:string = 'http://localhost:9003/api/transporter/';
    Uri: string = 'http://13.127.193.228:9003/api/transporter/';
    // Uri2:string = 'http://13.127.193.228:9001/api/transporter/'
    // Uri2: string = 'http://13.232.41.205:9003/api/transporters/';

    constructor(
        private http: Http
        // private http: AuthHttp
        , private session: SessionService) {
    }
    // Get all Transporter's Trucks list
    public getDetails(id: number): Observable<any> {
        debugger;
        var requestOptions = new RequestOptions();
        requestOptions.headers = new Headers();
        // console.log("localstorage",localStorage.getItem(TOKEN_NAME))
        // requestOptions.headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        // requestOptions.headers.append('Authorization', 'Bearer ' + localStorage.getItem(TOKEN_NAME));
        // requestOptions.headers.append('Accept', 'application/json');
        // requestOptions.headers.append('Content-Type', 'application/json');


        let myHeaders = new Headers();
        myHeaders.append('Authorization', 'Bearer ' + localStorage.getItem(TOKEN_NAME));
        let myParams = new URLSearchParams();
        let options = new RequestOptions({ headers: myHeaders, params: myParams });
        console.log("Options", options);
        return this.http.get(`${this.Uri}${id}`, options)
            .map(response => {
                let transporter = response.json() as Transporter;
                this.session.User = transporter;
                return transporter.trucks || [];
            })
            .catch(error => {
                let msg = error._body;
                throw Observable.of(msg);
            });
    }
    public getTrucksPosition(): Observable<boolean> {
        var requestOptions = new RequestOptions();
        requestOptions.headers = new Headers({ 'Content-Type': 'application/json' });
        requestOptions.headers.append('Authorization', 'Bearer ' + localStorage.getItem(TOKEN_NAME));
        let deviceIds = new Array<string>();
        this.session.User.trucks.forEach(truck => {
            deviceIds.push(truck.deviceId);
        });
        console.log("Device Ids: " + deviceIds);
        var self = this;
        return this.http.post(`${this.Uri}trucks_position`, deviceIds, requestOptions)
            .map(response => {
                let trucks = response.json() as Array<Truck>;
                trucks.forEach(truck => {
                    self.session.User.trucks.find(T => T.deviceId == truck.deviceId).location = truck.location;
                    console.log(`Positions${truck.deviceId}: ${truck.location}`);
                });
                return true;
            });
    }
    public getSingleTruckPosition(deviceId): Observable<any> {
        var requestOptions = new RequestOptions();
        requestOptions.headers = new Headers({ 'Content-Type': 'application/json' });
        requestOptions.headers.append('Authorization', 'Bearer ' + localStorage.getItem(TOKEN_NAME));
        var self = this;
        console.log("Location Data :", `${this.Uri}track/${deviceId}`);
        debugger;
        return this.http.get(`${this.Uri}track/${deviceId}`, requestOptions)
            .map(response => {
                let reqResponse:any = response["_body"];
                let locationLatLong = reqResponse.split(",");
                // console.log("Call ME");
                // return '';
                // return response.text();
                return locationLatLong;
            }).catch(error => {
                throw Observable.of(error._body);
            });
    }
}