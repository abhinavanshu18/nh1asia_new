import { Injectable} from '@angular/core';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { Http, Response, RequestOptions, Headers, URLSearchParams } from '@angular/http'; 
import { HttpClient, HttpHeaders } from '@angular/common/http'; 
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import { Transporter } from '../model/transporter';
import { catchError, map, tap } from 'rxjs/operators';
import { SessionService } from './session.service';
import {TOKEN_AUTH_PASSWORD, TOKEN_AUTH_USERNAME, TOKEN_NAME} from '../services/auth.constant';
import { JwtHelper } from 'angular2-jwt';

@Injectable()
export class AuthService {
    // Uri:string = 'http://localhost:9003/api/transporter/';
    Uri:string = 'http://13.127.193.228:9003/api/transporter/';
    // static AUTH_TOKEN = 'http://13.127.193.228:9999/oauth/token';

    UUID: any = "";
    user:Transporter = null;
    Token:string = "";
    Self: AuthService;
    jwtHelper: JwtHelper = new JwtHelper();
    constructor(private http: Http, private httpClient : HttpClient
                , private uniqueDeviceID: UniqueDeviceID
                , private session: SessionService) {
        // For callbacks
        this.Self = this;
        var wrapper = this;
        this.uniqueDeviceID.get()
                .then((uuid: any) => { wrapper.UUID = uuid; console.log(uuid)})
                .catch((error: any) => console.log(error));
    }
    
    public verifyPhoneNo(contactNo: string): Observable<any> {
        var requestOptions = new RequestOptions();
        requestOptions.headers = new Headers({'Content-Type' : 'application/x-www-form-urlencoded'});
        return this.http.post(`${this.Uri}auth`, `contactNumber=${contactNo}&muid=${this.UUID}`, requestOptions)
        .map(response => {
            return response.text();
        }).catch(error => {
            let msg = "Failed, please retry!";
            if (error.status === 404) {
            msg = "Invalid Contact no.";
            }
            else if (error.status === 417) {
            msg = "Failed to verify, please retry!";
            }
            throw Observable.of(msg);
        });
    }

    public verifyOtp(otp: string,contactNo: string): Observable<any> {
        var requestOptions = new RequestOptions();
        requestOptions.headers = new Headers({'Content-Type' : 'application/x-www-form-urlencoded'});
        requestOptions.headers.append('Authorization', 'Basic ' + btoa(TOKEN_AUTH_USERNAME + ':' + TOKEN_AUTH_PASSWORD));

        return this.http.post(`${this.Uri}verify`,`contactNumber=${contactNo}&otp=${otp}`,  requestOptions)
                .map(response => {
                    // extract token
                    let data = response.json();
                    this.Token = data.access_token;
                    const decodedToken = this.jwtHelper.decodeToken(this.Token);
                    localStorage.setItem(TOKEN_NAME, this.Token);
    
                    return data;
                })
                // .catch((status)=> this.handleError(status));
                .catch(error => {
                    let msg = error._body;
                    throw Observable.of(msg);
                });
                        // .map( this.handleOtpResponse );

        // return this.httpClient.post<Transporter>(`${this.Uri}verify`,
        //                 `contactNumber=${contactNo}&otp=${otp}`, { 'headers': headers})
        //                 .map( response => { return response.json(); });
    }

    // private handleOtpResponse(response: Response) : Transporter {
    //     // extract token
    //     this.Token = response.headers.get('access_token');
    //     Self.session.User = response.json();
    //     return this.session.User;
    // }
}